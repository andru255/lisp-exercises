;; To create this game, we need to write three functions:
;; guess-my-number
;; smaller
;; bigger
;;
;; Defining global variables
;; in the top level definitions
;; with defparameter function "

(defparameter *small* 1)
(defparameter *big* 100)


;; Defining global functions
(defun guess-my-number ()
  ;; Ash buildt-in function deal with bits
  (ash (+ *small* *big*) -1)
)

(print "Executing (guess-my-number)--> ")
(print (guess-my-number))

;; Defining the smaller and bigger functions
(defun smaller()
  (setf *big* (1- (guess-my-number)))
)

(defun bigger()
  (setf *small* (1- (guess-my-number)))
  (guess-my-number)
)

;; Defining the start-over function

(defun start-over()
  (defparameter *small* 1)
  (defparameter *big* 100)
  (guess-my-number)
)

(print "Executing (start-over)--> ")
(print (start-over))
