;; Basic datatypes in lisp

;;Symbols
;;Are fundamental, are case insensitive

(princ "Applying eq between two symbols")
(princ "(eq 'Foo 'foo) :")
(princ (eq 'Foo 'foo))


;;Numbers
;;Floating-point numbers and integers.
(print "sum:")
(print "(+ 1 1.0)")
(print (+ 1 1.0))

(print "sustraction:")
(print "(- 1 9)")
(print (- 1 9))

(print "multiply:")
(print "(* 1 3)")
(print (* 1 3))

(print "divide:")
(print "check the difference:")
(print "(/ 2 4)")
(print (/ 2 4))

(print "(/ 2 4.0)")
(print (/ 2 4.0))

(print "exponential:")
(print "(expt 4 2)")
(print (expt 4 2))

;;Strings
;;All with "" its considerated String
(print "Hi I'm a String")
;;All with "" its considerated String
(print "Printin with escaped caracters...")
(princ "Double quotes \"everywhere..\"")
