;; Lists
;; Are crucial feature in lisp
;; The lisp program architecture its made for lists
;; lists in Lisp are held together by structures called cons cells

;; Using the cons function
(print (cons 'chicken 'cat))

;; cons return a single object, the cons cell, represented by
;; parentheses and a dot between the two connected items.

;; Don't confuse this with a regular list. The dot in the middle
;; makes this a cons cell, just linking those two items together.

;; Notice how we prefix our two pieces of data with a single quote
;; to make sure that Lisp sees them as just data and doesn't try to evaluate them as code.
;; If instead of another piece of data, we attach the symbol nil on the right side of the list
;; something special happens:

(print (cons 'chicken 'nil))

;; or check that

(print (cons 'chicken ()))

;; The empty list (), can be used interchangeably with the nil symbol in CLISP.
;; The cons function also can add a new item to the front of the list.

(print (cons 'pork '(beef chicken)))

;; In that example, we consed pork to a list containing beef and chicken.
;; Since all lists are made of cons cells, our (beef chicken) list must have been
;; created from its own two cons cells, perhaps like this:

(print (cons 'beef (cons 'chicken ())))

;; Combining the previous two examples, we can see what all the lists look
;; like when viewed as conses. This is what is really happening:

(print (cons 'pork (cons 'beef (cons 'chicken ()))))

;; Either response would have been perfectly correct. In Lisp, a chain of cons cells
;; and a list are exactly the same thing.


;; The car and cdr functions
;; The car function gets the first thing out of the first slot of a cell:
(print (car '(pork beef chicken)))

;; The cdr function is used to grab the value out of the second slot, or the
;; remainder of a list
(print (cdr '(pork beef chicken)))

;; other examples working with car and cdr
(print (car (cdr '(pork beef chicken))))

;; the method cadr
(print (cadr '(pork beef chicken)))

;; the list function
(print (list 'pork 'beef 'chicken))

;; NOTE: Remember that there is difference between a list created with the list
;; function, one created by specifying individual cons cells, or once created in
;; data mode using the single quote; They're all the same animal.

;; THE SAME RESULT say:
(print (cons 'PORK (cons 'BEEF (cons 'CHICKEN ()))))
(print (list 'PORK 'BEEF 'CHICKEN))
(print '(PORK BEEF CHICKEN))

;; Nested lists
(print '(cat (duck cat) ant))
