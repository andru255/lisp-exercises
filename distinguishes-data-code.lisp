;; Code mode
;; By default the REPL understand do you want to execute a command
;; (command param1 param2 param3)
;; (foo bla bla bla)

;; Simple Calculation
;; 2 ^ 3 = 8
;; In lisp must be:
(print (expt 2 3))

;;or
(print (expt 2 (+ 3 4)))


;; Data mode
;; This means the computer will not try to "execute it"

(print '(expt 2 3))
