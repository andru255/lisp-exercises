;; set variables
(setq my-name "myFirstVariable")
;;`C-xC-e' => "Bastien" (displayed in the mini-buffer)

;;`insert' will insert "Hello!" where the cursor is:
(insert "Hello!")
;;`C-xC-e' => "Hello!"

;;`insert' will insert "Hello!" where the cursor is:
(insert "Hello!" "world!")
;;`C-xC-e' => "Hello! world!"

;; You can use variables instead of strings:
(insert "Hello, I am " my-name)
;;`C-xC-e' => "Hello, I am myFirstVariable"

;; You can combine sexps into functions:
(defun hello()
  (insert "Hello, I am " my-name )
)
;; `C-xC-e' => hello

;; You can evaluate functions:
(hello)
;; `C-xC-e' => Hello, I am myFirstVariable

;; The empty parentheses in the function's definition means that
;; it does not accept arguments.  But always using `my-name' is
;; boring, let's tell the function to accept one argument (here
;; the argument is called "name"):

(defun hello (name) (insert "Hello " name))
;; `C-xC-e' => hello

;; Now let's call the function with the string "you" as the value
;; for its unique argument:
(hello "you")
;; `C-xC-e' => "Hello you"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Now switch to a new buffer named "*test*" in another window:

(switch-to-buffer-other-window "*test*")
;; `C-xC-e' => hello
;; => [screen has two windows and cursor is in the *test* buffer]

;; Mouse over the top window and left-click to go back.  Or you can
;; use `C-xo' (i.e. hold down control-x and hit o) to go to the other
;; window interactively.

;; You can combine several sexps with `progn':
(progn
  (switch-to-buffer-other-window "*test*")
  (hello "you")
)

;; `C-xC-e'
;; => [The screen has two windows and cursor is in the *test* buffer]

;; Now if you don't mind, I'll stop asking you to hit `C-xC-e': do it
;; for every sexp that follows.

;; Always go back to the *scratch* buffer with the mouse or `C-xo'.

;; It's often useful to erase the buffer:
(progn
  (switch-to-buffer-other-window "*test*")
  (erase-buffer)
  (hello "there")
)

;; Or to go back to the other window:
(progn
  (switch-to-buffer-other-window "*test*")
  (erase-buffer)
  (hello "you")
  (other-window 1)
)

;; You can bind a value to a local variable with `let':
(let ((local-name "you"))
  (switch-to-buffer-other-window "*test*")
  (erase-buffer)
  (hello local-name)
  (other-window 1)
)

;; No need to use `progn' in that case, since `let' also combines
;; several sexps.

;; Let's format a string:
(format "Hello %s!\n" "visitor")

;; %s is a place-holder for a string, replaced by "visitor".
;; \n is the newline character.

;; Let's refine our function by using format:
(defun hello(name)
  (insert (format "Hello %s!\n" name))
)

(hello "you")

;; Let's create another function which uses `let':
(defun greeting(name)
  (let ((your-name "Dru"))
    (insert (format "Hello %s!\n\nI am %s" name your-name))
  )
)

;; And evaluate it:
(greeting "you")

;; Some function are interactive:
(read-from-minibuffer "Enter your name: ")

;; Evaluating this function returns what you entered at the prompt.

;; Let's make our `greeting' function prompt for your name:
(defun gretting (from-name)
  (let (your-name (read-from-minibuffer "Enter your name: ")))
  (insert (format "Hello!\n\nI am %s and you are %s" from-name your-name))
)

(greeting "xD")

(defun gretting(from-name)
  (let (your-name (read-from-minibuffer "Enter your name: ")))
  (switch-to-buffer-other-window "*test*")
  (erase-buffer)
  (insert (format "Hello %s!\n\nI am %s." your-name from-name))
  (other-windows 1)
)

;; Now test it:
(greeting "blabla")


;; Take a breath.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Let's store a list of names:
(setq list-of-names '("Sarah" "Chloe" "Mathilde"))

;; Get the first element of this list with `car':
(car list-of-names)

;; Get a list of all but the first element width `cdr':
(cdr list-of-names)

;; Add an element to the beginning of a list with `push':
(push "Stephanie" list-of-names)

;; NOTE: `car' and `cdr' don't modify the list, but `push' does.
;; This is an important difference: some functions don't have any
;; side-effects (like `car') while others have (like `push').

;; Let's call `hello' for each element in `list-of-names':
(mapcar 'hello list-of-names)

;; Refine `greeting' to say hello to everyone in `list-of-names':
(defun gretting ()
  (switch-to-buffer-other-window "*test*")
  (erase-buffer)
  (mapcar 'hello list-of-names)
  (other-window 1)
)

(gretting)
